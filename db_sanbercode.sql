-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Okt 2022 pada 14.21
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sanbercode`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(4, '2022_06_29_070520_tb_jenis_obat', 1),
(5, '2022_06_29_070550_tb_penerimaan_obat', 1),
(6, '2022_06_29_090217_obats', 1),
(7, '2022_07_27_090601_create_tb_user_table', 1),
(8, '2022_07_30_100620_t_transaksi_gudang', 1),
(9, '2022_07_30_100637_t_det_transaksi_gudang', 1),
(10, '2022_08_07_035422_t_obat_polindes', 1),
(11, '2022_08_07_045359_t_obat_farmasi', 1),
(12, '2022_10_01_051931_m_polindes', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_jenis_obat`
--

CREATE TABLE `m_jenis_obat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `m_jenis_obat`
--

INSERT INTO `m_jenis_obat` (`id`, `type`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'jenis_obat_0', NULL, NULL, NULL),
(2, 'jenis_obat_1', NULL, NULL, NULL),
(3, 'jenis_obat_2', NULL, NULL, NULL),
(4, 'jenis_obat_3', NULL, NULL, NULL),
(5, 'jenis_obat_4', NULL, NULL, NULL),
(6, 'jenis_obat_5', NULL, NULL, NULL),
(7, 'jenis_obat_6', NULL, NULL, NULL),
(8, 'jenis_obat_7', NULL, NULL, NULL),
(9, 'jenis_obat_8', NULL, NULL, NULL),
(10, 'jenis_obat_9', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_polindes`
--

CREATE TABLE `m_polindes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `m_polindes`
--

INSERT INTO `m_polindes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Polindes Trutup', NULL, NULL),
(2, 'Polindes Kepohagung', NULL, NULL),
(3, 'Polindes Kesamben', NULL, NULL),
(4, 'Polindes  Cangkring', NULL, NULL),
(5, 'Polindes  Sumurjalak', NULL, NULL),
(6, 'Polindes  Plumpang', NULL, NULL),
(7, 'Polindes  Ngrayung', NULL, NULL),
(8, 'Polindes  Sumberagung', NULL, NULL),
(9, 'Polindes  Magersari', NULL, NULL),
(10, 'Polindes  Jatimulyo', NULL, NULL),
(11, 'Polindes  Penidon', NULL, NULL),
(12, 'Pustu  Kepohagung', NULL, NULL),
(13, 'Pustu  Magersari', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_det_transaksi_gudang`
--

CREATE TABLE `t_det_transaksi_gudang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_transaksi_gudang` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_kadaluarsa` date NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sumber_dana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `t_det_transaksi_gudang`
--

INSERT INTO `t_det_transaksi_gudang` (`id`, `id_transaksi_gudang`, `name`, `jumlah`, `tanggal_kadaluarsa`, `satuan`, `jenis_obat`, `sumber_dana`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'obat 7', '1', '2022-10-12', 'kaplet', 'jenis_obat_7', 'Pemerintah', NULL, NULL, NULL),
(2, 1, 'obat 9', '2', '2022-10-12', 'kaplet', 'jenis_obat_9', 'Pemerintah', NULL, NULL, NULL),
(3, 2, 'obat 8', '1', '2022-10-12', 'kaplet', 'jenis_obat_8', 'Pemerintah', NULL, NULL, NULL),
(4, 3, 'obat 9', '5', '2022-10-12', 'kaplet', 'jenis_obat_9', 'Pemerintah', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_obat`
--

CREATE TABLE `t_obat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_kadaluarsa` date NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_obat_id` bigint(20) UNSIGNED NOT NULL,
  `sumber_dana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `t_obat`
--

INSERT INTO `t_obat` (`id`, `name`, `jumlah`, `tanggal_kadaluarsa`, `satuan`, `jenis_obat_id`, `sumber_dana`, `tanggal`, `keterangan`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'obat 0', '1', '2022-10-12', 'kaplet', 1, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, NULL),
(2, 'obat 1', '2', '2022-10-12', 'kaplet', 2, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, NULL),
(3, 'obat 2', '3', '2022-10-12', 'kaplet', 3, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, NULL),
(4, 'obat 3', '4', '2022-10-12', 'kaplet', 4, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, NULL),
(5, 'obat 4', '5', '2022-10-12', 'kaplet', 5, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, NULL),
(6, 'obat 5', '6', '2022-10-12', 'kaplet', 6, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, NULL),
(7, 'obat 6', '7', '2022-10-12', 'kaplet', 7, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, NULL),
(8, 'obat 7', '7', '2022-10-12', 'kaplet', 8, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, '2022-10-06 20:31:52'),
(9, 'obat 8', '8', '2022-10-12', 'kaplet', 9, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, '2022-10-06 20:42:50'),
(10, 'obat 9', '3', '2022-10-12', 'kaplet', 10, 'Pemerintah', '2022-10-12', 'test', NULL, NULL, '2022-10-09 05:02:52'),
(11, 'Fikri Ahmad Dwi Nasrulloh', '12', '2022-10-28', 'blister', 1, 'APBD', '2022-10-07', 'fsdfsd', NULL, '2022-10-06 20:31:05', '2022-10-06 20:31:05'),
(12, 'covid', '10', '2022-10-15', 'tablet', 1, 'JKN', '2022-10-04', 'Covid', NULL, '2022-10-09 05:04:41', '2022-10-09 05:04:41'),
(13, 'mixagrip', '20', '2022-10-15', 'tablet', 6, 'APBD', '2022-10-09', 'tugas TA', NULL, '2022-10-09 05:30:04', '2022-10-09 05:30:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_obat_farmasi`
--

CREATE TABLE `t_obat_farmasi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_kadaluarsa` datetime NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sumber_dana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `t_obat_farmasi`
--

INSERT INTO `t_obat_farmasi` (`id`, `name`, `jumlah`, `tanggal_kadaluarsa`, `satuan`, `jenis_obat`, `sumber_dana`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'obat 7', '1', '2022-10-12 00:00:00', 'kaplet', 'jenis_obat_7', 'Pemerintah', NULL, '2022-10-06 20:31:52', '2022-10-06 20:31:52'),
(2, 'obat 9', '2', '2022-10-12 00:00:00', 'kaplet', 'jenis_obat_9', 'Pemerintah', NULL, '2022-10-06 20:31:52', '2022-10-06 20:31:52'),
(3, 'obat 8', '1', '2022-10-12 00:00:00', 'kaplet', 'jenis_obat_8', 'Pemerintah', NULL, '2022-10-06 20:42:50', '2022-10-06 20:42:50'),
(4, 'obat 9', '5', '2022-10-12 00:00:00', 'kaplet', 'jenis_obat_9', 'Pemerintah', NULL, '2022-10-09 05:02:52', '2022-10-09 05:02:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_obat_polindes`
--

CREATE TABLE `t_obat_polindes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `polindes_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_kadaluarsa` date NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sumber_dana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_penerimaan_obats`
--

CREATE TABLE `t_penerimaan_obats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sumber_dana` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` datetime NOT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_transaksi_gudang`
--

CREATE TABLE `t_transaksi_gudang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lokasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `t_transaksi_gudang`
--

INSERT INTO `t_transaksi_gudang` (`id`, `lokasi`, `tanggal`, `keterangan`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, '0', '2022-10-07', 'Buku tabungan', NULL, '2022-10-06 20:31:52', '2022-10-06 20:31:52'),
(2, '0', '2022-10-07', 'vhg vjg', NULL, '2022-10-06 20:42:50', '2022-10-06 20:42:50'),
(3, '0', '2022-10-10', 'Covid', NULL, '2022-10-09 05:02:52', '2022-10-09 05:02:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_user`
--

CREATE TABLE `t_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_jenis_obat`
--
ALTER TABLE `m_jenis_obat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_polindes`
--
ALTER TABLE `m_polindes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `t_det_transaksi_gudang`
--
ALTER TABLE `t_det_transaksi_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `t_obat`
--
ALTER TABLE `t_obat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `t_obat_farmasi`
--
ALTER TABLE `t_obat_farmasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `t_obat_polindes`
--
ALTER TABLE `t_obat_polindes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `t_penerimaan_obats`
--
ALTER TABLE `t_penerimaan_obats`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `t_transaksi_gudang`
--
ALTER TABLE `t_transaksi_gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `t_user_username_unique` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `m_jenis_obat`
--
ALTER TABLE `m_jenis_obat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `m_polindes`
--
ALTER TABLE `m_polindes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `t_det_transaksi_gudang`
--
ALTER TABLE `t_det_transaksi_gudang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `t_obat`
--
ALTER TABLE `t_obat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `t_obat_farmasi`
--
ALTER TABLE `t_obat_farmasi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `t_obat_polindes`
--
ALTER TABLE `t_obat_polindes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `t_penerimaan_obats`
--
ALTER TABLE `t_penerimaan_obats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `t_transaksi_gudang`
--
ALTER TABLE `t_transaksi_gudang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `t_user`
--
ALTER TABLE `t_user`
  MODIFY `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
